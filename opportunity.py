# This file is part of the sale_weight module for Tryton.
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.

from trytond.model import fields
from trytond.pool import PoolMeta


class CrmOpportunityLine(metaclass=PoolMeta):
    __name__ = "crm.opportunity.line"
    unit_price = fields.Numeric('Unit Price', digits=(16, 2))

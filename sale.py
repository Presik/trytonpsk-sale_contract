# This file is part of the sale_weight module for Tryton.
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from decimal import Decimal
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval

conversor = None
try:
    from numword import numword_es
    conversor = numword_es.NumWordES()
except Exception:
    print("Warning: Does not possible import numword module!")
    print("Please install it...!")


__all__ = ['Sale']

STATES = {
    'readonly': Eval('state') != 'draft',
}

STATES_LINE = {
    'readonly': Eval('state') != 'pending',
}

_ZERO = Decimal('0.0')


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'
    contract_line = fields.Many2One('sale.contract.line',
        'Contract Line', select=True, states=STATES)
    validity_contract = fields.Char('Validity Contract',
            depends=['state'], states=STATES)
    start_contract = fields.Date('Start Contract',
            depends=['state'], states=STATES)
    end_contract = fields.Date('End Contract',
            depends=['state'], states=STATES)
    total_amount_words = fields.Function(fields.Char('Total Amount Words'),
            'get_total_amount_words')

    def get_total_amount_words(self, name=None):
        if self.total_amount and conversor:
            num = (conversor.cardinal(int(self.total_amount))).upper()
            return num
